import React, { Component } from 'react';

class LiveGenForm extends Component {
	constructor(props) {
		super(props);
	    this.handleChange = this.handleChange.bind(this);
	    this.state = { 
	    	data: undefined,
	    	privacy_settings: ['EVERYONE', 'ALL_FRIENDS', 'FRIENDS_OF_FRIENDS', 'SELF', 'CUSTOM']
	    }
	}

	componentDidMount() {
		this.setState({
			data: this.props.data
		});
	}

	handleChange(e) {
	   let target = e.target;
	   let value = target.value;
	   let name = target.name;
	   this.props.handleChange(name, value);
	}

	render() {
		let d = this.props.data;

		let privacy_options = this.state.privacy_settings.map((p, i) => <option key={i} value={p}>{p}</option>);

		if (this.props.loggedIn) {
		 return (
		 	<div className="live-generator-form">
				<form onSubmit={this.props.submitDetails}>
					<label htmlFor="title">Title (optional):</label>
					<input type="text" name="title" onChange={this.handleChange} value={d.title} />
					<br/>
					<label htmlFor="description">Description:</label>
					<textarea name="description"  onChange={this.handleChange} value={d.description} />
					<br/>
					<label htmlFor="privacy.value">Privacy:</label>
					<select value={d.privacy.value} onChange={this.handleChange} name="privacy.value">
						{privacy_options}
					</select>
					<br/>
					<label htmlFor="save_vod">Save video after session ends?</label>
					<select value={d.save_vod} onChange={this.handleChange} name="save_vod">
						<option value={true}>Yes</option>
						<option value={false}>No</option>
					</select>
					<br />
					<button>Go Live</button>
				</form>
				<hr/>
			</div>
		  );
		} else {
			return <h1>Please log in.</h1>;
		}
	}
}

export default LiveGenForm;